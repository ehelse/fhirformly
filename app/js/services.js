'use strict';

/* Services */

var fhirServices = angular.module('fhirServices', ['ngResource']);


fhirServices.factory("Observation", function ($resource) {
    return $resource(
        FHIR_URL+'/Observation',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        },
        {
            // If you want to implement special functions, you'd
            // put them here.
        }
    );
});


fhirServices.factory("Questionnaire", function ($resource) {
    return $resource(
        FHIR_URL+'/Questionnaire/:id',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        },
        {
            'update': { method:'PUT' }
        }
    );
});

fhirServices.factory("ConvertForm", function ($resource) {
    return $resource(
        FHIR_URL+'/Questionnaire/:id',
        {
            // If you're passing variables, for example into the URL
            // they would be here and then as :varName in the URL
        },
        {
            // If you want to implement special functions, you'd
            // put them here.
        }
    );
});

