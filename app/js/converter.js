/**
 * Created by LarsKristian on 02.09.2015.
 */

var converterService = angular.module('Converter', []);

converterService.service('FHIR2Formly', function () {
        
        var converter = this;

        converter.createFormlyModel = function (json, formlyForm) {
            if (typeof formlyForm === 'undefined') {
                formlyForm = [];
            }

            if (typeof json.concept !== 'undefined') {
                var header = {
                    template: '<h2>'+json.concept[0].display+'</h2>'
                };
                formlyForm.push(header);
            }

            if (typeof json.group !== 'undefined') {
                //.log('Found group: ' + JSON.stringify(json));
                // the group variable is defined and contains an array
                return converter.createFormlyModel(json.group, formlyForm);
            } else {
                //console.log('Did not find group: ' + JSON.stringify(json));
                return converter.expandGroup(json, formlyForm);
            }
        };
        converter.expandGroup = function (json, formlyForm) {
            if (Array.isArray(json)) {
                //console.log('Found an array: ' + JSON.stringify(json));
                var arrayLength = json.length;
                //console.log('Arraylength: ' + arrayLength);

                for (var i = 0; i < arrayLength; i++) {
                    //console.log('Array-Item[' + i + ']' + JSON.stringify(json[i]));

                    if (typeof json[i].text !== 'undefined') {
                        var header = {
                            template: '<h3>'+json[i].text+'</h3>'
                        };
                        formlyForm.push(header);
                    }

                    if (typeof json[i].title !== 'undefined') {
                        var header = {
                            template: '<h3>'+json[i].title+'</h3>'
                        };
                        formlyForm.push(header);
                    }

                    if (typeof json[i].question !== 'undefined') {
                        var questionArray = json[i].question;
                        var questionArrayLength = json[i].question.length
                        //console.log('Array has a question array of ' + questionArrayLength + ' items.' + JSON.stringify(questionArray));
                        for (var qi = 0; qi < questionArrayLength; qi++) {
                            var questionArrayItem = questionArray[qi];
                            var question = {};
                            var rand = Math.floor((Math.random() * 1000000) + 1);
                            question.key = questionArrayItem.text.split(" ").join("").replace('?', '')+rand;
                            questionArrayItem.linkId = question.key;
                            question.templateOptions = {};

                            if (typeof questionArrayItem.type !== 'undefined') {
                                //console.log('Type set to '+questionArrayItem.type);
                                if (questionArrayItem.type == 'integer') {
                                    question.templateOptions.type = 'number';
                                    question.type = 'input';
                                } else if (questionArrayItem.type == 'date') {
                                    question.templateOptions.type = 'date';
                                    question.type = 'input';
                                } else if (questionArrayItem.type == 'boolean') {
                                    question.templateOptions.type = 'checkbox';
                                    question.type = 'checkbox';
                                } else if (questionArrayItem.type == 'choice') {
                                    question.templateOptions.type = 'choice';
                                    question.type = 'select';

                                    var containedArray = converter.questionnaire.contained;
                                    var containedArrayLength = containedArray.length;
                                    //console.log('Contain array has a question array of ' + containedArrayLength + ' items.');
                                    for (var ci = 0; ci < containedArrayLength; ci++) {
                                        var contained = containedArray[ci];
                                        if (contained.id == questionArrayItem.options.reference) {
                                            console.log('Found concept:'+contained.id);
                                            var fhirOptArray = contained.compose.include[0].concept;
                                            var options = [];
                                            var optionArrayLength = fhirOptArray.length;
                                            //console.log('Array has a question array of ' + questionArrayLength + ' items.' + JSON.stringify(questionArray));
                                            for (var oi = 0; oi < optionArrayLength; oi++) {
                                                var singleOpt = {};
                                                var fhirOpt = fhirOptArray[oi];
                                                singleOpt.name = fhirOpt.display;
                                                singleOpt.value = fhirOpt.code;
                                                options.push(singleOpt);
                                            }
                                            question.templateOptions.options = options;
                                            //console.log("Options:"+JSON.stringify(options));
                                        }
                                    }
                                    /*
                                     console.log('Choice found... '+questionArrayItem.options.reference);
                                     var fhirOptArray = converter.questionnaire.contained.filter(function(item) { return item.id === questionArrayItem.options.reference; })[0].compose.include[0].concept;
                                     var options = [];
                                     var optionArrayLength = fhirOptArray.length;
                                     //console.log('Array has a question array of ' + questionArrayLength + ' items.' + JSON.stringify(questionArray));
                                     for (var i = 0; i < optionArrayLength; i++) {
                                     var singleOpt = {};
                                     var fhirOpt = fhirOptArray[i];
                                     singleOpt.name = fhirOpt.display;
                                     singleOpt.value = fhirOpt.code;
                                     options.push(singleOpt);
                                     }
                                     */

                                } else {
                                    question.templateOptions.type = questionArrayItem.type;
                                    question.type = 'input';
                                }
                            }
                            else {
                                question.templateOptions.type = 'text';
                                question.type = 'input';
                            }

                            question.templateOptions.label = questionArrayItem.text;
                            question.templateOptions.placeholder = questionArrayItem.text;
                            question.templateOptions.required = false;
                            //question.question = questionArrayItem.text;
                            formlyForm.push(question);
                            //console.log('Adding a question: ' + questionArrayItem.text);
                        }
                    }
                }

            } else {
                //console.log('Did not find an array: ' + JSON.stringify(json));
                //return formlyForm;
            }

            return formlyForm;

        };

        converter.formly = {};
        converter.formly.data = [
            {
                key: 'email',
                type: 'input',
                templateOptions: {
                    type: 'email',
                    label: 'Email address',
                    placeholder: 'Enter email'
                }
            }];

        converter.getFormly = function() {
            return converter.formly;
        };

        converter.convertFromFHIR = function (str) {
            converter.questionnaire = JSON.parse(str);
            converter.questionnaireString = str;

            converter.formly.data = converter.createFormlyModel(converter.questionnaire);

            return converter.formly;
        };

    });


converterService.service('Formly2FHIR', function () {

    var converter = this;

    // Create QuestionnaireAnswer
    converter.createQuestionnaireAnswer = function() {
        console.log('Creating answer');
        converter.questionnaireAnswerString = JSON.stringify(converter.questionnaire);
        $.each(converter.model, function(key,val){
            console.log('Processing key='+key+' which has value '+val);
            var replaceThis = '"linkId":"'+key+'"';
            var replaceWith = '"answer": [{"valueString":"'+val+'"}],"linkId":"'+key+'"';
            converter.questionnaireAnswerString = converter.questionnaireAnswerString.replace(replaceThis,replaceWith);
            console.log(replaceThis+' <--- '+replaceWith);
            console.log('Result='+converter.questionnaireAnswerString);
        });
        converter.questionnaireAnswer = JSON.parse(converter.questionnaireAnswerString);

        // Remove unwanted, and required.

        converter.questionnaireAnswer.questionnaire = 'link to questionnaire';
        converter.questionnaireAnswer.status = 'completed';
        converter.questionnaireAnswer.subject = {"reference": "/Patient/1"};
        converter.questionnaireAnswer.author = {"reference": "/Patient/1"};
        converter.questionnaireAnswer.authored = new Date();
        converter.questionnaireAnswer.source = {"reference": "/Patient/1"};
        delete converter.questionnaireAnswer.publisher;
        delete converter.questionnaireAnswer.telecom;
    };

});