console.clear(); // <-- keep the console clean on refresh

var FHIR_URL = 'http://fhir.roland.bz';

	var app = angular.module('formlyExample', ['ngRoute', 'formly', 'formlyBootstrap','fhirServices','ngResource','Converter'], function config(formlyConfigProvider) {
		// set templates here
		formlyConfigProvider.setType({
			name: 'custom',
			templateUrl: 'custom.html'
		});
	});


    app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
                when('/list', {templateUrl: 'partials/list.html' }).
                when('/form/:formId', {templateUrl: 'partials/form.html' }).
                when('/edit/:formId', {templateUrl: 'partials/edit.html' }).
                otherwise({
                    redirectTo: '/list'
                });
        }]);


//angular.module('myApp.controllers', []).controller('MyCtrl1', ['$scope', '$rootScope', '$location', '$timeout', '$q', '$interval', 'bleservice',
//    function ($scope, $rootScope, $location, $timeout, $q, $interval, bleservice)


    app.controller('MainCtrl', function MainCtrl($scope, $http, $routeParams, $location, formlyVersion, Questionnaire, Observation, FHIR2Formly) {

        console.log('$routeParams:'+JSON.stringify($routeParams));

        $scope.questionnaireString = '{"resourceType":"Questionnaire","id":"f201","text":{"status":"generated","div":"<div>!-- Snipped for Brevity --></div>"},"status":"published","date":"2010","contained":[{"resourceType":"ValueSet","id":"malefemale","name":"MaleFemale","description":"Just a male-female option test","copyright":"None","status":"active","compose":{"include":[{"system":"aint-no-system-here","concept":[{"code":"Male","display":"Male"},{"code":"Female","display":"Female"}]}]}},{"resourceType":"ValueSet","id":"yesno","name":"MaleFemale","description":"Just a male-female option test","copyright":"None","status":"active","compose":{"include":[{"system":"aint-no-system-here","concept":[{"code":"Yes","display":"Yes"},{"code":"No","display":"No"}]}]}}],"group":{"concept":[{"system":"http://example.org/system/code/lifelines/nl","code":"VL 1-1, 18-65_1.2.2","display":"Lifelines Questionnaire 1 part 1"}],"group":[{"question":[{"text":"Do you have allergies?","linkId":"Doyouhaveallergies358912","type":"choice","options":{"reference":"yesno"}}]},{"text":"General questions","question":[{"text":"What is your gender?","type":"choice","linkId":"Whatisyourgender91910","options":{"reference":"malefemale"}},{"text":"What is your date of birth?","linkId":"Whatisyourdateofbirth122221","type":"date"},{"text":"What is your country of birth?","linkId":"Whatisyourcountryofbirth429075"},{"text":"What is your marital status?","linkId":"Whatisyourmaritalstatus19494"}]},{"title":"Intoxications","question":[{"text":"Do you smoke?","linkId":"Doyousmoke441147"},{"text":"Do you drink alchohol?","linkId":"Doyoudrinkalchohol607969"}]}]}}';

        /*
        $scope.questionnaire = JSON.parse($scope.questionnaireString);
        $scope.questionnaireString = JSON.stringify($scope.questionnaire, null, "\t");
        */

        // FHIR connection stuff

        $scope.url = window.location.href;

        $scope.token = "";

        FHIR.oauth2.ready(function (smart) {

            console.log("smart="+JSON.stringify(smart));
            $scope.token = smart.server.auth.token;
            $http.defaults.headers.common['Authorization']= 'Bearer '+$scope.token;

            $scope.debug = JSON.stringify(smart.server);

            $scope.smart = smart;
            $scope.patient = smart.context.patient;
            console.log("Patient=" + JSON.stringify($scope.patient));

            $scope.patient.read().then(function (pt) {

                $scope.thisPatient = pt;
                //console.log("Patient="+JSON.stringify(pt));

                $scope.$apply(function () {
                    $scope.name =
                        pt.name[0].given.join(" ") + " " +
                        pt.name[0].family.join(" ");
                });

                //$scope.$apply($location.path('/'));
                $scope.loadInitial();

            });
        });

        $scope.selectedFormId = 0;

        $scope.viewForm = function(formId) {
            console.log('formId:'+formId);
            $scope.selectedFormId = formId;
            $scope.questionnaire = Questionnaire.get({ id:formId }, function () {
                console.log('$scope.debug='+JSON.stringify($scope.debug));
                $scope.questionnaireString = JSON.stringify($scope.questionnaire);
                FHIR2Formly.convertFromFHIR($scope.questionnaireString);
            });
            $location.path('/form/'+formId);
        };

        $scope.editForm = function(formId) {
            console.log('formId:'+formId);
            $scope.selectedFormId = formId;
            $scope.questionnaire = Questionnaire.get({ id:formId }, function () {
                console.log('$scope.debug='+JSON.stringify($scope.debug));
                $scope.questionnaireString = JSON.stringify($scope.questionnaire, null, "\t");
                FHIR2Formly.convertFromFHIR($scope.questionnaireString);
            });
            $location.path('/edit/'+formId);
        };

        $scope.updateForm = function(str, formId) {
            Questionnaire.update({ id:formId }, JSON.parse(str), function () {
                console.log('Form saved ok: '+JSON.stringify($scope.saved));
                $location.path('/list');
            });
        };

        $scope.saveAsNewForm = function(str) {
            Questionnaire.save({ id:'' }, JSON.parse(str), function () {
                console.log('Form created ok: '+JSON.stringify($scope.saved));
                $scope.questionnaireList = Questionnaire.get({ id:'' }, function () {
                    $location.path('/list');
                });
            });
        };

        $scope.generatedFormFields = FHIR2Formly.getFormly();

        $scope.loadForm = function() {
            FHIR2Formly.convertFromFHIR($scope.questionnaireString);
            //console.log('Result from convertFromFHIR: '+FHIR2Formly.convertFromFHIR($scope.questionnaireString));
            //$scope.generatedFormFields = FHIR2Formly.getFormly();
        };

        $scope.loadInitial = function() {

            $scope.questionnaireList = Questionnaire.get({ id:'' }, function () {
                //console.log('$scope.questionnaireList='+JSON.stringify($scope.questionnaireList));
            });

            JSON.stringify(FHIR2Formly.convertFromFHIR($scope.questionnaireString));

        };

        $scope.onSubmit = onSubmit;

		// variable assignment
        $scope.author = { // optionally fill in your info below :-)
			name: 'Lars Roland',
			url: 'https://twitter.com/kentcdodds' // a link to your twitter/github/blog/whatever
		};

        $scope.env = {
			angularVersion: angular.version.full,
			formlyVersion: formlyVersion
		};

        $scope.model = {
		};

        $scope.formFields = [{
			key: 'text',
			type: 'input',
			templateOptions: {
				label: 'Text',
				placeholder: 'Formly is terrific!'
			}
		}, {
			key: 'story',
			type: 'textarea',
			templateOptions: {
				label: 'Some sweet story',
				placeholder: 'It allows you to build and maintain your forms with the ease of JavaScript :-)'
			}
		}];

		// function definition
		function onSubmit() {
            //$scope.createQuestionnaireAnswer();
			//alert(JSON.stringify($scope.model), null, 2);
		}





    });


	app.directive('exampleDirective', function() {
		return {
			templateUrl: 'example-directive.html'
		};
	});

